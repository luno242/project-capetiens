var express = require('express');
var bodyParser = require('body-parser')
var urlencodedParser = bodyParser.urlencoded({ extended: false })
const fs = require('fs');
var server = express();

server.get('/', function(request, response) {  
	response.sendfile('./main.html');
});
server.get('/hugues-capet', function(request, response) {
	response.sendfile('./hugues-capet.html');
});
server.get('/louis-ix', function(request, response) {
	response.sendfile('./louis-ix.html');
});
server.get('/philippe-auguste', function(request, response) {
	response.sendfile('./philippe-auguste.html');
});
server.get('/philippe-le-bel', function(request, response) {
	response.sendfile('./philippe-le-bel.html');
});
server.get('/formulaire', function(request, response) {
	response.sendfile('./formulaire.html');
});

//in terminal
//server.post('/formulaire', urlencodedParser, function (request, response) {
   // console.log(request.body);
//});   
 
//to json
server.post('/formulaire', urlencodedParser, function (request, response) {
    response.send(request.body);
    let data = JSON.stringify(request.body, null, 2);
    //write new json
    /*fs.writeFile('newCandidat.json', data, (err) => {
        if (err) throw err;
        console.log('Data written to file');*/
    //append add new candidats in json    
    fs.appendFile('newCandidat.json',"\n"+ data,(err) => {
        if(err) throw err;
        console.log('Data written to file');    
    });  
});
console.log('This is after the write call');  


server.use(express.static('image')); //static package pour montre img, ccs et js dans site
server.use(express.static('css'));
server.use(express.static('js'));
server.listen(8050);// express contain server http

/*var express = require ("express");
var app = express ();

app.get("/",function(reg, res){
    res.render ("homepage.ejs");
})

app.get("/capet/:capetTitre/:capetParagraph/",function(reg, res){
    
    const titre = reg.params.capetTitre;
    const paragraph= reg.params.capetParagraph;
    res.render ("huge-capet.ejs",{
        titre: capet,
        paragraph: paragraph
      
    }
    );
})

app.listen('8080', function(){
    console.log ('web online');
})*/





/*var http = require('http');
var url = require('url');
var querystring = require('querystring');// dokaze selektovat ruzne parametry v retezci

var server = http.createServer(function(req, res) {
    var params = querystring.parse(url.parse(req.url).query); //vytvori tabulku paramentu ve kterych si muzu vybrat napr. jmeno: params ['prenom']
    res.writeHead(200, {"Content-Type": "text/plain"});
    if ('prenom' in params && 'nom' in params) {
        res.write('Vous vous appelez ' + params['prenom'] + ' ' + params['nom']);
    } //http://localhost:8080/?prenom=Lucie&nom=Cabelka vrati jmeno a prijmeni na strankucd
    else {
        res.write('Vous devez bien avoir un prénom et un nom, non ?');
    }
    res.end();
});
server.listen(8080);*/



    //pro primou tvorbu stranky na serveru
    /*res.writeHead(200, {"Content-Type": "text/html"});
    res.write('<!DOCTYPE html>'+
'<html>'+
'    <head>'+
'        <meta charset="utf-8" />'+
'        <title>Les Capetiens</title>'+
'    </head>'+ 
'    <body>'+
'     	<header>'+
'           <h1>Les Capétiens</h1>' +
'       </header>'+
'    </body>'+
'</html>');*/
   